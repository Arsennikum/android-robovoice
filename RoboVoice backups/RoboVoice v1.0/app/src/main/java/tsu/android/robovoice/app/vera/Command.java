package tsu.android.robovoice.app.vera;

/**
 * Created by Алексей on 04.03.2016.
 */
public class Command{// implements Comparable<Command> {

    public final int id;
    public final String name;
    public final String responseOnExecute;

    public Command(int id, String name, String responseOnExecute) {
        this.id = id;
        this.name = name;
        this.responseOnExecute = responseOnExecute;
    }

    public Command(int id, String name) {
        this(id, name, null);
    }

//    public static enum Category {
//        Cat1, Switch, Dimmer, Thermostat("temperature");
//
//        public final String field;
//
//        Category(String field) {
//            this.field = field;
//        }
//
//        Category() {
//            this(null);
//        }
//    }
//
//    public final Category category;
//    public final Map<String, Object> data = new HashMap<String, Object>();
//
//    public Command(int id, String name, Category category) {
//        this.id = id;
//        this.name = name;
//        this.category = category;
//    }
//
//    @Override
//    public int compareTo(Command another) {
//        return another.name.length() - this.name.length();
//    }
//
//    public static Command fromJSON(JSONObject json, SparseArray<String> categories) {
//        try {
//            String cat = categories.get(json.getInt("category"));
//            if (cat == null) return null;
//            Category category = Category.valueOf(cat);
//            Command command = new Command(json.getInt("id"), json.getString("name").toLowerCase(), category);
//            JSONArray names = json.names();
//            for (int i = 0; i < names.length(); i++) {
//                String name = names.getString(i);
//                command.data.put(name, json.get(name));
//            }
//            return command;
//        } catch (Exception e) {
//            return null;
//        }
//    }
}
