package tsu.android.robovoice.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.*;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;
import org.apache.commons.io.FileUtils;
import tsu.android.robovoice.app.recognizer.DataFiles;
import tsu.android.robovoice.app.recognizer.Grammar;
import tsu.android.robovoice.app.recognizer.PhonemeMapper;
import tsu.android.robovoice.app.vera.Command;
import tsu.android.robovoice.app.vera.Controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class MainActivity extends Activity implements RecognitionListener {

    /**
     * Инициализация микрофона, прослушки hotword и сенсора, и вызов соотв. прорисовки экрана <br/>
     * Это лучше как-нибудь сделать, чтобы вызывалось один раз при запуске <br/>
     */
    private void init() {
        setupController();
        sensorListener = new SensorListener();

        // mTextToSpeech вроде пока не нужен нигде.
        mTextToSpeech = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.ERROR) return;
                if (mTextToSpeech.isLanguageAvailable(Locale.getDefault()) == TextToSpeech.LANG_AVAILABLE) {
                    mTextToSpeech.setLanguage(Locale.getDefault());
                }
                mTextToSpeech.setOnUtteranceCompletedListener(mUtteranceCompletedListener);
            }
        });
        sensorListener = new SensorListener();

        drawState.listenHotword();
    }

    private class BluetoothManager { // Неплохо бы добавить синглтон. Почему не сделал всё static - не были бы доступны поля MyActivity
        BluetoothAdapter bt;
        //static private final String btRobotName = "Lenovo A536";
        String macBtAddress;
        private BluetoothDevice roboBt;
        private ArrayList<BluetoothDevice> foundDevices = new ArrayList<BluetoothDevice>();
        private BluetoothController _btCtrl;

        //ArrayAdapter<String> mArrayAdapter;
        // Создаем BroadcastReceiver для ACTION_FOUND
        private final BroadcastReceiver btDevicesReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent){
                String action= intent.getAction();
                // Когда найдено новое устройство
                if(BluetoothDevice.ACTION_FOUND.equals(action)){
                    // Получаем объект BluetoothDevice из интента
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    onBtDeviceFound(device);
                }
            }
        };

        void initBT() throws InterruptedException {
            bt = BluetoothAdapter.getDefaultAdapter();
            if (bt == null) {
                Toast.makeText(MainActivity.this, "Программа не обнаружила Bluetooth в вашем устройстве!!",
                        Toast.LENGTH_SHORT).show();
                // TODO: 05.03.2016 exit app
            }

            if (!bt.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 0);
            }
            int sec = 0;
            while (!bt.isEnabled()) {
                if (sec > 10) {
                    Toast.makeText(MainActivity.this,
                            "Не могу работать с выключенным Bluetooth, включите его и попробуйте снова",
                            Toast.LENGTH_SHORT).show();
                    //TODO: exit app
                }
                Thread.sleep(1000);
                Toast.makeText(MainActivity.this,
                        "Жду, пока вы включите bluetooth", Toast.LENGTH_SHORT).show();
                sec++;
            }
        }

        private void onBtDeviceFound(BluetoothDevice device) {
            Log.v("Bluetooth discovery:", device.getName() + " " + device.getAddress());
            foundDevices.add(device);
            /*if (device.getName().equalsIgnoreCase(btRobotName)) {
                setRoboBt(device);
            }*/
        }

//        BtExchangeController getBtExchange() {
//            BtExchangeController btExchangeController = new BtExchangeController(bt, lockBtDevice);
//            return btExchangeController;
//        }
//
//        public void setRoboBt(BluetoothDevice robot) {
//            roboBt = robot;
//            _btCtrl = new BluetoothController(bt, roboBt);
//        }
//
//        boolean setRoboBtFromMemory {
//            preference... or:
//            if ((macBtAddress != null) && (!macBtAddress.isEmpty())) {
//                setRoboBt(bt.getRemoteDevice(macBtAddress));
//                return;
//            }
//        }

        void startDiscoveryBt() {
            Set<BluetoothDevice> pairedDevices = bt.getBondedDevices();
            // Если список спаренных устройств не пуст
            if (pairedDevices.size() > 0) { // нужна ли эта проверка вообще или for догадается?
                // проходимся в цикле по этому списку
                for (BluetoothDevice device : pairedDevices) {
                    onBtDeviceFound(device);
                }
            }

            // Регистрируем BroadcastReceiver
            IntentFilter filter=new IntentFilter(BluetoothDevice.ACTION_FOUND);

            registerReceiver(btDevicesReceiver, filter); // Не забудьте снять регистрацию в onDestroy - ok
            bt.startDiscovery();
        }

        private void sendCmdToRobot(int cmdId) {
            Log.d("send msg: ", Integer.toString(cmdId));
            // _btCtrl.sendMsgFromEnum(cmdId); // TODO: 08.03.2016 todo when debug with arduino
        }

        @Override
        protected void finalize() throws Throwable {
            unRegBtReceiver();
            super.finalize();
        }

        void unRegBtReceiver() {
            unregisterReceiver(btDevicesReceiver);
        }

        public boolean connectTo(int idFromFoundDevices) {
            if (true) return true; // TODO: 08.03.2016 todo when debug with arduino
            try {
                _btCtrl = new BluetoothController(bt, foundDevices.get(idFromFoundDevices));
            } catch (IOException e) {
                return false;
            }
            return true;
        }
    }

    private class DrawState {
        private static final String TAG = "DrawState";

        public void init() {
            Log.v(TAG, "init");
            micStateText.setText(R.string.mic_state_text_init);
        }

        public void error() {
            micStateText.setText(R.string.mic_state_text_Error);
        }

        public void result() {
            Log.v(TAG, "result");
            //mMicView.setBackgroundResource(R.drawable.background_big_mic);
        }

        public void listenHotword(){
            micStateText.setText(R.string.mic_state_text_listenHotword);
        }

        public void listenCmd() {
            Log.v(TAG, "listenCmd");
            micStateText.setText(R.string.mic_state_text_listenCmd);
            //mMicView.setBackgroundResource(R.drawable.background_big_mic_green);
        }

        public void stopListen() {
            Log.v(TAG, "stopListen");
            //mMicView.setBackgroundResource(R.drawable.background_big_mic);
        }
    }

    private class SensorListener implements SensorEventListener {
        private SensorManager mSensorManager;
        private float mSensorMaximum;
        private float mSensorValue;

        public SensorListener() {
            mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
            if (sensor != null) {
                mSensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_FASTEST);
                mSensorMaximum = sensor.getMaximumRange();
            }
        }

        /**
         * Перегрузка из интерфейса SensorEventListener, когда закрываем датчик приближения
         * @param event
         */
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (MainActivity.this.recognition == null) return;
            mSensorValue = event.values[0];
            if (mSensorValue < mSensorMaximum) {
                post(500, new Runnable() {
                    @Override
                    public void run() {
                        if (mSensorValue < mSensorMaximum) {
                            MainActivity.this.recognition.startCmdRecognition();
                        }
                    }
                });
            } else recognition.stopCmdRecognition();
        }

        private void onDestroy() {
            mSensorManager.unregisterListener(this);
        }

        /**
         * Перегрузка из интерфейса SensorEventListener
         * @param accuracy
         */
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }

    private class Recognition {

        private SpeechRecognizer mRecognizer;

        private void startStopRecognition() {
            if (mRecognizer == null) return;
            if (LISTEN_HOTWORD_SEARCHNAME.equals(mRecognizer.getSearchName())) {
                startCmdRecognition();
            } else {
                stopCmdRecognition();
            }
        }

        private synchronized void startCmdRecognition() {
            if (mRecognizer == null || LISTEN_CMDS_SEARCHNAME.equals(mRecognizer.getSearchName())) return;
            mRecognizer.cancel();
            new ToneGenerator(AudioManager.STREAM_MUSIC, ToneGenerator.MAX_VOLUME).startTone(ToneGenerator.TONE_CDMA_PIP, 200);
            post(400, new Runnable() {
                @Override
                public void run() {
                    drawState.listenCmd(); // TODO: 28.02.2016 убрать drawState в MainActivity
                    mRecognizer.startListening(LISTEN_CMDS_SEARCHNAME, 3000);
                    post(4000, mStopRecognitionCallback);
                }
            });

        }

        private void startHotwordListening() {
            if (LISTEN_HOTWORD_SEARCHNAME.equals(mRecognizer.getSearchName())) { return; }
            mRecognizer.startListening(LISTEN_HOTWORD_SEARCHNAME);
            drawState.listenHotword();
        }

        private synchronized void stopCmdRecognition() {
            if (mRecognizer == null || LISTEN_HOTWORD_SEARCHNAME.equals(mRecognizer.getSearchName())) return;
            mRecognizer.stop();
            drawState.stopListen();
        }

        private void setupRecognizer() {
            if (mController == null) return; // redundant Перед вызовом выполняется. Однако входные данные нужно проверять...
            final String hotword = getString(R.string.hotword_for_init_listening);
            new AsyncTask<Void, Void, Exception>() {
                @Override
                protected Exception doInBackground(Void... params) {
                    try {
                        PhonemeMapper phonMapper = new PhonemeMapper(getAssets().open("dict/ru/hotwords"));
                        Grammar grammar = new Grammar(mController.getCommandNames(), phonMapper);
                        grammar.addWords(hotword);
                        DataFiles dataFiles = new DataFiles(getPackageName(), "ru");
                        File hmmDir = new File(dataFiles.getHmm());
                        File dict = new File(dataFiles.getDict());
                        File jsgf = new File(dataFiles.getJsgf());
                        copyAssets(hmmDir);
                        saveToFile(jsgf, grammar.getJsgf());
                        saveToFile(dict, grammar.getDict());
                        mRecognizer = SpeechRecognizerSetup.defaultSetup()
                                .setAcousticModel(hmmDir)
                                .setDictionary(dict)
                                .setBoolean("-remove_noise", false)
                                .setKeywordThreshold(1e-7f)
                                .getRecognizer();
                        // Добавляет слово для поиска
                        mRecognizer.addKeyphraseSearch(LISTEN_HOTWORD_SEARCHNAME, hotword);
                        // Добавляет список команд
                        mRecognizer.addGrammarSearch(LISTEN_CMDS_SEARCHNAME, jsgf);
                    } catch (IOException e) {
                        return e;
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Exception ex) {
                    if (ex != null) {
                        onRecognizerSetupError(ex);
                    } else {
                        onRecognizerSetupComplete();
                    }
                }
            }.execute();

        }

        private void onRecognizerSetupComplete() {
            //Toast.makeText(MainActivity.this, "Ready", Toast.LENGTH_SHORT).show();
            mRecognizer.addListener(MainActivity.this);
            startHotwordListening();
        }

        private void onRecognizerSetupError(Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

        private void copyAssets(File baseDir) throws IOException {
            String[] files = getAssets().list("hmm/ru");

            for (String fromFile : files) {
                File toFile = new File(baseDir.getAbsolutePath() + "/" + fromFile);
                InputStream in = getAssets().open("hmm/ru/" + fromFile);
                FileUtils.copyInputStreamToFile(in, toFile);
            }
        }

        private void saveToFile(File f, String content) throws IOException {
            File dir = f.getParentFile();
            if (!dir.exists() && !dir.mkdirs()) {
                throw new IOException("Cannot create directory: " + dir);
            }
            FileUtils.writeStringToFile(f, content, "UTF8");
        }

        public void stopAll() {
            if (mRecognizer != null)
                mRecognizer.stop();
        }

        private void onDestroy() {
            if (mRecognizer != null) mRecognizer.cancel();
        }
    }

    //region inner classes
    private DrawState drawState = new DrawState();
    private SensorListener sensorListener;
    private Recognition recognition = new Recognition();
    private BluetoothManager bluetoothUtil = new BluetoothManager();
    //endregion

    //region constants
    private static final String LOG_TAG = "MainActivity";
    private static final String LISTEN_CMDS_SEARCHNAME = "command";
    private static final String LISTEN_HOTWORD_SEARCHNAME = "hotword";
    //endregion

    private final Handler mHandler = new Handler();
    private final Queue<String> mSpeechQueue = new LinkedList<String>();

    private Controller mController;
    private TextToSpeech mTextToSpeech;

    private final Runnable mStopRecognitionCallback = new Runnable() {
        @Override
        public void run() {
            MainActivity.this.recognition.stopAll();
        }
    };

    private final TextToSpeech.OnUtteranceCompletedListener mUtteranceCompletedListener = new TextToSpeech.OnUtteranceCompletedListener() {
        @Override
        public void onUtteranceCompleted(String utteranceId) {
            synchronized (mSpeechQueue) {
                mSpeechQueue.poll();
                if (mSpeechQueue.isEmpty()) {
                    recognition.startHotwordListening();
                }
            }
        }
    };

    public void btnConnectClick(final View view) {
        final String[] devices = new String[bluetoothUtil.foundDevices.size()];
        for (int i = 0; i < bluetoothUtil.foundDevices.size(); i++) {
            devices[i] = bluetoothUtil.foundDevices.get(i).getName() + "\r\n" + bluetoothUtil.foundDevices.get(i).getAddress();
        }


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Выберите BT").setItems(devices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (bluetoothUtil.connectTo(which)) {
                    view.setEnabled(false);
                    view.setVisibility(View.INVISIBLE);
                    init();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Не удаётся подключиться к данному устройсту",
                            Toast.LENGTH_SHORT).show();
                }
            }
        }).setCancelable(true).create().show();
    }

    TextView micStateText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        micStateText = (TextView) findViewById(R.id.micStateText);
        drawState.init();
        try {
            bluetoothUtil.initBT();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        bluetoothUtil.startDiscoveryBt();
    }

    @Override
    protected void onDestroy() {
        recognition.onDestroy();

        mTextToSpeech.stop();
        mTextToSpeech.shutdown();
        super.onDestroy();
    }

    private void setupController() {
        new AsyncTask<Void, Void, Controller>() {
            @Override
            protected Controller doInBackground(Void... params) {
                Controller controller = new Controller();
                return controller; //.initialize() ? controller : null;
            }

            @Override
            protected void onPostExecute(Controller controller) {
                mController = controller;
                if (controller == null) {
                    Log.v(LOG_TAG, "Controller is not found");
                    Toast.makeText(MainActivity.this, "Controller is not found", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(MainActivity.this, "Controller is found! Please wait...", Toast.LENGTH_SHORT).show();
                    recognition.setupRecognizer();
                }
            }
        }.execute();
    }

    @Override
    public void onBeginningOfSpeech() {
        Log.d(LOG_TAG, "onBeginningOfSpeech");
    }

    @Override
    public void onEndOfSpeech() {
        Log.d(LOG_TAG, "onEndOfSpeech");
        /*if (LISTEN_CMDS_SEARCHNAME.equals(mRecognizer.getSearchName())) {
            mRecognizer.stop();
        }*/
        recognition.stopCmdRecognition();
    }

    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null) return;
        String text = hypothesis.getHypstr(); // нужно попроверять параметры гепотезы
        int prob = hypothesis.getProb();
        int bestScore = hypothesis.getBestScore();

        recognition.startCmdRecognition();
        Log.d(LOG_TAG, "partial result: " + text);
    }

    @Override
    public void onResult(Hypothesis hypothesis) {
        drawState.result();
        mHandler.removeCallbacks(mStopRecognitionCallback);
        String text = hypothesis != null ? hypothesis.getHypstr() : null;
        Log.d(LOG_TAG, "onResult " + text);
        if (text != null) {
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show();

            executeCmds(text);
        }
        recognition.startHotwordListening();
    }

    @Override
    public void onError(Exception e) {
        Log.v(LOG_TAG, "RecognitionListener override onError");
        drawState.error();
    }

    @Override
    public void onTimeout() {
        Log.v(LOG_TAG, "MainActivity timeout");
    }

    private void post(long delay, Runnable task) {
        mHandler.postDelayed(task, delay);
    }

    private void executeCmds(final String text) {
        new AsyncTask<String, Void, List<Command>>() {
            @Override
            protected List<Command> doInBackground(String... params) {
                return mController.parseCommands(params[0]);
            }

            @Override
            protected void onPostExecute(List<Command> commands) {
                for (Command command : commands) {
                    bluetoothUtil.sendCmdToRobot(command.id);
                    String result = mController.process(command);
                    if (result != null) {
                        Toast.makeText(MainActivity.this, result, Toast.LENGTH_SHORT).show();
                        speak(result);
                    }
                }
            }
        }.execute(text);
    }

    private void speak(String text) {
        synchronized (mSpeechQueue) {
            recognition.stopAll();
            mSpeechQueue.add(text);
            HashMap<String, String> params = new HashMap<String, String>(2);
            params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, UUID.randomUUID().toString());
            params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_MUSIC));
            params.put(TextToSpeech.Engine.KEY_FEATURE_NETWORK_SYNTHESIS, "true");
            mTextToSpeech.speak(text, TextToSpeech.QUEUE_ADD, params);
        }

    }
}
