package tsu.android.robovoice.app.vera;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Алексей on 04.03.2016.
 */
public class CommandsManager {

    private static final String TAG = "CommandsManager";

    //private final Uri mDetectUri = Uri.parse("http://cp.mios.com/detect_unit.php");
    //private final DefaultHttpClient httpClient = new DefaultHttpClient();
    //private final Executor executor = Executors.newSingleThreadExecutor();
    //private Uri mBaseUri;

    private final List<Command> commands;

    public CommandsManager() {
        commands = new ArrayList<Command>();
        commands.add(new Command("0", "садись", "лампочка села"));
        commands.add(new Command("1", "встань", "энергия поднимается"));
        commands.add(new Command("2", "повернись кругом", "Скоро голова закружится"));
        commands.add(new Command("31", "иди вперёд", "хорошо"));
        commands.add(new Command("32", "иди назад", "Сделано"));
        commands.add(new Command("33", "повернись влево", "Вас понял"));
        commands.add(new Command("34", "повернись вправо", "Наше дело правое, победа будет за нами!"));
    }

    public String[] getCommandNames() {
        String[] commandNames = new String[commands.size()];
        for (int i = 0; i < commandNames.length; i++) {
            commandNames[i] = commands.get(i).name;
        }
        return commandNames;
    }

    public List<Command> parseCommands(String str) {
        List<Command> commands = getCommands();
        List<Command> result = new ArrayList<Command>();
        //Collections.sort(commands); Зачем? Comparable убрал - теперь не работает, но и, думаю, не нужно
        for (Command command : commands) {
            String s = str.replace(command.name, "");
            if (!s.equals(str)) {
                str = s;
                result.add(command);
            }
        }
        return result;
    }

    public List<Command> getCommands() {
        return commands;
        /*
        if (mBaseUri == null) return Collections.emptyList();
        JSONObject json = getJsonObject(mBaseUri.buildUpon().appendQueryParameter("codeToSend", "sdata").appendQueryParameter("output_format", "json").build());
        if (json == null) return Collections.emptyList();
        try {
            JSONArray categories = json.getJSONArray("categories");
            JSONArray array = json.getJSONArray("commands");
            if (array.length() == 0 || categories.length() == 0) return Collections.emptyList();
            SparseArray<String> cats = new SparseArray<String>(categories.length());
            for (int i = 0; i < categories.length(); i++) {
                JSONObject cat = categories.getJSONObject(i);
                cats.append(cat.getInt("codeToSend"), cat.getString("name"));
            }
            List<Command> commands = new ArrayList<Command>(array.length());
            for (int i = 0; i < array.length(); i++) {
                Command command = Command.fromJSON(array.getJSONObject(i), cats);
                if (command != null) {
                    commands.add(command);
                }
            }
            return commands;
        } catch (JSONException e) {
            return Collections.emptyList();
        }*/
    }

    public String process(final Command command) {
        Log.v(TAG, "process" + command.name); // + ", data:" + command.data);
        return command.responseOnExecute;
        /*if (command.category.field != null) {
            Object obj = command.data.get(command.category.field);
            return obj == null ? null : obj.toString();
        }else {
            Log.v(TAG, "process, command.category.field == null");
            return null;
        }

        executor.execute(new Runnable() {
            @Override
            public void run() {
                toggle(command);
            }
        });
        }*/
    }

    /*
    public boolean initialize() {
        Log.e(TAG, "initialize");

        JSONArray array = getJsonArray(mDetectUri);
        if (array == null || array.length() == 0) return false;
        try {
            String address = array.getJSONObject(0).getString("InternalIP");
            mBaseUri = Uri.parse("http://" + address + ":3480/data_request");
            return true;
        } catch (JSONException e) {
            return false;
        }
    }

    private void toggle(Command command) {
        String status = (String) command.data.get("status");
        status = "1".equals(status) ? "0" : "1";
        get(mBaseUri.buildUpon().appendQueryParameter("codeToSend", "action")
                .appendQueryParameter("DeviceNum", command.codeToSend+"")
                .appendQueryParameter("serviceId", "urn:upnp-org:serviceId:SwitchPower1")
                .appendQueryParameter("action", "SetTarget")
                .appendQueryParameter("newTargetValue", status).build());
    }

    private String get(Uri uri) {
        final String url = uri.toString();
        HttpGet get = new HttpGet(url);
        try {
            HttpResponse response = httpClient.execute(get);
            if (response.getStatusLine().getStatusCode() != 200) {
                response.getEntity().consumeContent();
                return null;
            }
            return EntityUtils.toString(response.getEntity(), "UTF-8");
        } catch (IOException e) {
            Log.e(TAG, "Can't execute [" + url + "]", e);
            return null;
        }
    }


    private JSONObject getJsonObject(Uri uri) {
        String json = get(uri);
        try {
            return json != null ? new JSONObject(json) : null;
        } catch (JSONException e) {
            return null;
        }
    }

    private JSONArray getJsonArray(Uri uri) {
        String json = get(uri);
        try {
            return json != null ? new JSONArray(json) : null;
        } catch (JSONException e) {
            return null;
        }
    }*/
}
