package tsu.android.robovoice.app;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;

import java.io.*;
import java.util.UUID;

/**
 * Created by Алексей on 06.03.2016.
 */
public class BluetoothController {
    private final static String TAG = "BluetoothController";

    private class BluetoothIOThread extends Thread {
        final int RECEIVE_MESSAGE = 1;        // Статус для Handler
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private Handler handler;

        public BluetoothIOThread(BluetoothSocket socket) {

            handler = new Handler() {
                public void handleMessage(android.os.Message msg) {
                    switch (msg.what) {
                        case RECEIVE_MESSAGE:
                            String receivedMsg = msg.obj.toString();
                            Log.v("handleMessage", receivedMsg);
                            gotResponseMsg(receivedMsg);
                    }
                }
            };


            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[9];  // buffer store for the stream
            int bytes; // bytes returned from read()
            BufferedReader br = new BufferedReader(new InputStreamReader(mmInStream));

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    //bytes = mmInStream.read(buffer);        // Получаем кол-во байт и само собщение в байтовый массив "buffer"
                    //String test = buffer.toString();
                    int i = 0;
                    String readedString = br.readLine();
                    Log.v("br.readLine()", readedString);
                    handler.obtainMessage(RECEIVE_MESSAGE, readedString).sendToTarget();
                    //handler.obtainMessage(RECEIVE_MESSAGE, bytes, -1, buffer).sendToTarget();     // Отправляем в очередь сообщений Handler. Для взаимодействия из этого потока.

                } catch (IOException e) {
                    // mmInStream.close(); разве закрывать не нужно?? ок, там дальше есть, но в случае ошибки?
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String message) {
            Log.v(TAG, "...Данные для отправки: " + message + "...");
            byte[] msgBuffer = message.getBytes();
            try {
                mmOutStream.write(msgBuffer);
            } catch (IOException e) {
                Log.v(TAG, "...Ошибка отправки данных: " + e.getMessage() + "...");
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) { }
        }
    }

    private BluetoothSocket btSocket = null;
    // SPP UUID сервиса. Не понял какой именно и зачем
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothIOThread bluetoothIOThread;

    public BluetoothController(BluetoothAdapter adapter, BluetoothDevice device) throws IOException {
        Log.d(TAG, "BtExchangeController - попытка соединения...");

        // Set up a pointer to the remote node using it's address.
        //BluetoothDevice device1 = adapter.getRemoteDevice(device.getAddress()); FIXME

        // Two things are needed to make a connection:
        //   A MAC address, which we got above.
        //   A Service ID or UUID.  In this case we are using the UUID for SPP.
        try {
            btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            throw e;
            //Utils.exitApp("Fatal Error: In BtExchangeController - socket create failed: " + e.getMessage() + ".");
        }

        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.
        adapter.cancelDiscovery();

        // Establish the connection.  This will block until it connects.
        Log.d(TAG, "...Соединяемся...");
        try {
            btSocket.connect();
            Log.d(TAG, "...Соединение установлено и готово к передачи данных...");
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                throw e2;
                // Utils.exitApp("Fatal Error: In BtExchangeController - unable to close socket during connection failure" + e2.getMessage() + ".");
            }
            throw e;
        }

        // Create a data stream so we can talk to server.
        Log.d(TAG, "...Создание Socket...");

        bluetoothIOThread = new BluetoothIOThread(btSocket);
        bluetoothIOThread.start();
    }

    public void sendMsg(String cmdId) {
        bluetoothIOThread.write(cmdId);
    }

    private void gotResponseMsg(String msg) {
        Log.v("Respond msg", msg);
        // TODO: здесь может быть обработка входящих
    }
}
